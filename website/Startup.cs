﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AglTest.Startup))]
namespace AglTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
