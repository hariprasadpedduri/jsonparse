﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Collections.Generic;


namespace AglTest.Models
{
    public class OwnersModel
    {
        public List<JSONParser.Models.Owner> Owners { get; set; }
        
    }

}
