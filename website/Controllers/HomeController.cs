﻿using AglTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AglTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Test solution";

            JSONParser.OwnerJsonService service = new JSONParser.OwnerJsonService();
            var owners = new OwnersModel() { Owners = service.GetData()};

            return View(owners);
        }

        public ActionResult About()
        {
        

            return View();
        }

  
    }
}