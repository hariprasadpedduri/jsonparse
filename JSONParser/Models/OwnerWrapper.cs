﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSONParser.Models
{
    public class OwnerWrapper
    {
        public List<Owner> owners { get; set; }
    }
}
