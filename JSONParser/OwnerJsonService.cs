﻿using JSONParser.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace JSONParser
{
    public class OwnerJsonService
    {
     
        public List<Owner> GetData()
        {
            HttpClient client = new HttpClient();

            string json = null;

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            List<Owner> owners = null;
            var task = client.GetAsync("http://agl-developer-test.azurewebsites.net/people.json")     
                           .ContinueWith((taskwithresponse) =>
            {
                var response = taskwithresponse.Result;
                var jsonString = response.Content.ReadAsStringAsync();
                jsonString.Wait();
                json = jsonString.Result;
                owners = JsonConvert.DeserializeObject<List<Owner>>(jsonString.Result);

            });
            task.Wait();

            return owners;
        }
    }
}
